class AppDelegate < ProMotion::Delegate

  include SugarCube::Modal

  def on_load(app, options)
    UITabBar.appearance.setSelectedImageTintColor '#F59800'.to_color
    UINavigationBar.appearance.setTintColor '#F59800'.to_color


    open LoginScreen.new(nav_bar: true)
    self.window.tintColor = '#99763D'.to_color

    generate_key_and_session
  end

  def generate_key_and_session
    if App::Persistence[SESSION_ID].nil?
      KGStatusBar.showWithStatus("Loading")
      server.post("client/_ext_generate_key.php", client_platform: "I",
        device_token: "123456") do |result|
        KGStatusBar.dismiss
        # Check result status
        if result.object[:status] == STATUS_OK
          App::Persistence[SESSION_KEY] = result.object[:key]
          NSLog(result.object[:message])
        else
          KGStatusBar.showErrorWithStatus("Error connecting to the server.")
        end

      end
    end
  end

  # Singleton server throughout the app
  def server
    @server ||= AFMotion::Client.build(BASE_URL) do
      header "Accept", "application/json"
      operation :json
    end
  end

  # This method will handle session timeouts and etc automatically
  def make_post(url, params={}, &block)
    KGStatusBar.showWithStatus("Loading")
    server.post(url, params) do |result|

      # Update UI
      KGStatusBar.dismiss

      if result.error.nil?
        p result.object
        if result.object[:status] == STATUS_OK ||
          result.object[:status] == STATUS_ARRAY

          Motion::Blitz.success(result.object[:message])
          # All OK. Let callback handle
          block.call(result)
        elsif result.object[:status] == STATUS_EXPIRED
          Motion::Blitz.error(result.object[:message])
          App::Persistence[SESSION_ID] = nil
          App::Persistence[SESSION_KEY] = nil
          open_root_screen LoginScreen.new(nav_bar: true)
        else
          Motion::Blitz.error(result.object[:message])
        end
      else
        KGStatusBar.showErrorWithStatus "Sorry. An error occurred."
      end
    end
  end
end
