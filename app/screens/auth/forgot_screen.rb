class ForgotScreen < PM::FormotionScreen
  title "Forgot Password"

  def table_data
    {
      sections: [{
        rows: [{
          title: "Email",
          type: :email,
          placeholder: "john@doe.com",
          key: :email
        }]
      }, {
        rows: [{
          title: "Send",
          type: :submit
        }]
      }]
    }
  end

  def on_present
    self.navigationController.navigationBar.barTintColor = '#F59800'.to_color
    self.navigationController.navigationBar.tintColor = '#99763D'.to_color
  end

  def on_load
    self.form.on_submit do
      values = self.form.render

      KGStatusBar.showWithStatus("Submitting")
      UIApplication.sharedApplication.delegate.make_post("client/_ext_reset_password.php",
        key: App::Persistence[SESSION_KEY],
        user_name: values[:email]
        ) do |result|
        close
      end
    end
  end
end