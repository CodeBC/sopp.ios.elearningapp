class LoginScreen < PM::FormotionScreen

  include SugarCube::Modal

  title "Login"

  def view_did_load
    if Device.ios_version == "7.0"
      self.navigationController.navigationBar.barTintColor = '#F59800'.to_color
      self.navigationController.navigationBar.tintColor = '#99763D'.to_color
    end

    headerView = UIImageView.alloc.initWithFrame [[20, 40], [300, 83]]
    headerView.image = UIImage.imageNamed "sopp_logo"
    self.tableView.tableHeaderView = headerView
  end

  def table_data
    form = Formotion::Form.new
    form.build_section do |section|
      section.build_row do |row|
        row.title = "Username"
        row.type = :string
        row.placeholder = "johndoe"
        row.auto_correction = "no"
        row.auto_capitalization = "none"
        row.key = :username
      end

      section.build_row do |row|
        row.title = "Password"
        row.type = :string
        row.secure = true
        row.key = :password
      end
    end

    form.build_section do |section|
      section.build_row do |row|
        row.title = "Login"
        row.type = "submit"
        row.on_tap do |row|
          validate_and_login
        end
      end
    end

    form.build_section do |section|
      section.build_row do |row|
        row.title = "Forgot Password?"
        row.type = "submit"
        row.on_tap do |row|
          forgot_password
        end
      end
    end

    form
  end

  def on_appear
    unless App::Persistence[SESSION_ID].nil?
      start_main_screen
    end
  end

  def forgot_password
    open ForgotScreen.new
  end

  def validate_and_login
    values = self.form.render

    if values[:username] == ""
      Motion::Blitz.error 'Username cannot be blank'
    elsif values[:password] == ""
      Motion::Blitz.error 'Password cannot be blank'
    else
      login(values[:username], values[:password])
    end
  end

  def on_present
    self.navigationController.navigationBar.barTintColor = '#F59800'.to_color
    self.navigationController.navigationBar.tintColor = '#99763D'.to_color
  end

  # REST: LOGIN
  def login(username, password)
    UIApplication.sharedApplication.delegate.make_post("client/_ext_login.php",
      key: App::Persistence[SESSION_KEY],
      username: username,
      password: password) do |result|
      App::Persistence[SESSION_ID] = result.object[:session_id]
      start_main_screen
    end
  end

  def start_main_screen
    close
    open_tab_bar CoursesScreen.new(nav_bar: true), DiscussionScreen.new(nav_bar: true),
    ProfileScreen.new(nav_bar: true), SettingsScreen.new(nav_bar: true)
  end

end