class AssignmentScreen < PM::GroupedTableScreen

  refreshable

  attr_accessor :assignment
  items = nil

  def table_data
    @table_data ||= []
  end

  def on_load
    do_refresh
    self.title = @assignment[:item][:assignment_title]
  end

  def on_refresh
    do_refresh
  end

  def do_refresh
    UIApplication.sharedApplication.delegate.make_post("lesson/_ext_read_assignment.php",
      key: App::Persistence[SESSION_KEY],
      session_id: App::Persistence[SESSION_ID],
      assignment_id: @assignment[:assignment_id]) do |result|

      @items = result.object[:items]
      item = @items[0]

      @table_data = [{
        title: "Assignment Title",
        cells:[{
            title: item[:assignment_title]
        }]
      }, {
        title: "Assignment Deadline",
        cells:[{
            title: item[:assignment_deadline]
        }]
      }, {
        title: "Assignment Description",
        cells:[{
            title: item[:assignment_description] == "" ? item[:assignment_description] : "Not Available"
        }]
      }, {
        title: "Assignment Attachment(s)",
        cells: item[:assignment_upload_document].split(",").map do |doc|
          {
            title: doc,
            action: :tapped_doc,
            arguments: { item: doc }
          }
        end
      }]

      stop_refreshing
      update_table_data
    end
  end

  def on_present
    self.navigationController.navigationBar.barTintColor = '#F59800'.to_color
    self.navigationController.navigationBar.tintColor = '#99763D'.to_color
  end

  def tapped_doc(doc)
    webViewController = SVWebViewController.alloc.initWithAddress doc[:item]
    open webViewController
  end

end