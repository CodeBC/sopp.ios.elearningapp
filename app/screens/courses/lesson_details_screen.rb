class LessonDetailsScreen < PM::TableScreen

  refreshable
  searchable

  attr_accessor :lesson
  items = nil

  MIN_HEIGHT = 40

  def table_data
    @table_data ||= []
  end

  def on_load
    self.title = @lesson[:item][:lesson_name]
    do_refresh
  end

  def on_refresh
    do_refresh
  end

  def on_present
    self.navigationController.navigationBar.barTintColor = '#F59800'.to_color
    self.navigationController.navigationBar.tintColor = '#99763D'.to_color
  end

  def do_refresh
    UIApplication.sharedApplication.delegate.make_post("lesson/_ext_read_lesson.php",
      key: App::Persistence[SESSION_KEY],
      session_id: App::Persistence[SESSION_ID],
      lesson_id: @lesson[:item][:lesson_id]) do |result|

      @items = result.object[:items]
      @table_data = []

      unless @items[:material].nil?
        @table_data << {
          title: "Material",
          cells: @items[:material].map do |material|
            {
              title: material[:lesson_material_title],
              action: :tapped_material,
              height: calculate_height(material, 0),
              arguments: { item: material }
            }
          end
        }
      end

      unless @items[:assignment].nil?
        @table_data << {
          title: "Assignments",
          cells: @items[:assignment].map do |assignment|
            {
              title: assignment[:assignment_title],
              action: :tapped_assignment,
              height: calculate_height(assignment, 1),
              arguments: { item: assignment }
            }
          end
        }
      end

      stop_refreshing
      update_table_data
    end
  end

  def tableView(tableView, cellForRowAtIndexPath: indexPath)
    @reuseIdentifier ||= "SOPP_TITLE_CELL_IDENTIFIER"

    cell = tableView.dequeueReusableCellWithIdentifier(@reuseIdentifier) || begin
      UITableViewCell.alloc.initWithStyle(UITableViewCellStyleDefault, reuseIdentifier:@reuseIdentifier)
    end

    p 'Cells here'

    data = @table_data[indexPath.section][:cells][indexPath.row]

    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text = data[:title]
    cell.textLabel.font = UIFont.boldSystemFontOfSize 16
    cell.textLabel.numberOfLines = 0

    cell
  end

  # Helper method to calculate cell height
  def calculate_height(data, section)
    height = 0

    contentWidth = 320
    contentSize = CGSizeMake(contentWidth, 1200)

    font = UIFont.boldSystemFontOfSize(16)

    if section == 0
      height = height + (data[:lesson_material_title].sizeWithFont(font,
        constrainedToSize: contentSize, lineBreakMode: NSLineBreakByWordWrapping).height)
    else
      height = height + (data[:assignment_title].sizeWithFont(font,
        constrainedToSize: contentSize, lineBreakMode: NSLineBreakByWordWrapping).height)
    end

    if height < MIN_HEIGHT
      height = MIN_HEIGHT # min height
    end

    height + (10 * 2)
  end

  def tapped_material(material)
    link = material[:item][:lesson_material_uploadDocument]
    if material[:item][:lesson_material_uploadDocument].nil?
      link = material[:item][:lesson_material_websiteLink]
    end
    webViewController = SVWebViewController.alloc.initWithAddress link
    open webViewController
  end

  def tapped_assignment(assignment)
    open AssignmentScreen.new(assignment: assignment)
  end

end