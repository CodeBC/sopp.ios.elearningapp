class LessonsScreen < PM::TableScreen

  refreshable
  searchable

  MIN_HEIGHT = 40

  attr_accessor :mod
  items = nil

  def table_data
    @table_data ||= []
  end

  def on_load
    self.title = @mod[:item][:module_name]
    do_refresh
  end

  def on_refresh
    do_refresh
  end

  def on_present
    self.navigationController.navigationBar.barTintColor = '#F59800'.to_color
    self.navigationController.navigationBar.tintColor = '#99763D'.to_color
  end

  def do_refresh
    UIApplication.sharedApplication.delegate.make_post("lesson/_ext_retrieve_lesson_list.php",
      key: App::Persistence[SESSION_KEY],
      session_id: App::Persistence[SESSION_ID],
      module_id: @mod[:item][:module_id]) do |result|

      @items = result.object[:items]
      @table_data = [{
        cells: @items.map do |lesson|
          {
            title: lesson[:lesson_name],
            action: :tapped_item,
            height: calculate_height(lesson),
            arguments: { item: lesson }
          }
        end
      }]

      stop_refreshing
      update_table_data
    end
  end

  def tableView(tableView, cellForRowAtIndexPath: indexPath)
    @reuseIdentifier ||= "SOPP_TITLE_CELL_IDENTIFIER"

    cell = tableView.dequeueReusableCellWithIdentifier(@reuseIdentifier) || begin
      UITableViewCell.alloc.initWithStyle(UITableViewCellStyleDefault, reuseIdentifier:@reuseIdentifier)
    end

    data = @items[indexPath.row]

    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text = data[:lesson_name]
    cell.textLabel.font = UIFont.boldSystemFontOfSize 16
    cell.textLabel.numberOfLines = 0

    cell
  end

  # Helper method to calculate cell height
  def calculate_height(data)
    height = 0

    contentWidth = 320
    contentSize = CGSizeMake(contentWidth, 1200)

    font = UIFont.boldSystemFontOfSize(16)

    height = height + (data[:lesson_name].sizeWithFont(font,
      constrainedToSize: contentSize, lineBreakMode: NSLineBreakByWordWrapping).height)

    if height < MIN_HEIGHT
      height = MIN_HEIGHT # min height
    end

    height + (10 * 2)
  end

  def tapped_item(lesson)
    open LessonDetailsScreen.new(lesson: lesson)
  end

end