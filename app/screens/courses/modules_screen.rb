class ModulesScreen < PM::TableScreen

  refreshable
  searchable

  attr_accessor :course
  items = nil

  MIN_HEIGHT = 40

  def table_data
    @table_data ||= []
  end

  def on_load
    self.title = @course[:item][:course_name]
    do_refresh
  end

  def on_present
    self.navigationController.navigationBar.barTintColor = '#F59800'.to_color
    self.navigationController.navigationBar.tintColor = '#99763D'.to_color
  end

  def on_refresh
    do_refresh
  end

  def do_refresh
    UIApplication.sharedApplication.delegate.make_post("module/_ext_retrieve_registered_modules.php",
      key: App::Persistence[SESSION_KEY],
      session_id: App::Persistence[SESSION_ID],
      course_id: @course[:item][:course_id]) do |result|

      @items = result.object[:items]
      @table_data = [{
        cells: @items.map do |m|
          {
            title: m[:module_name],
            action: :tapped_item,
            height: calculate_height(m),
            arguments: { item: m }
          }
        end
      }]

      stop_refreshing
      update_table_data
    end
  end

  def tableView(tableView, cellForRowAtIndexPath: indexPath)
    @reuseIdentifier ||= "SOPP_TITLE_CELL_IDENTIFIER"

    cell = tableView.dequeueReusableCellWithIdentifier(@reuseIdentifier) || begin
      UITableViewCell.alloc.initWithStyle(UITableViewCellStyleDefault, reuseIdentifier:@reuseIdentifier)
    end

    data = @items[indexPath.row]

    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text = data[:module_name]
    cell.textLabel.font = UIFont.boldSystemFontOfSize 16
    cell.textLabel.numberOfLines = 0

    cell
  end

  # Helper method to calculate cell height
  def calculate_height(data)
    height = 0

    contentWidth = 320
    contentSize = CGSizeMake(contentWidth, 1200)

    font = UIFont.boldSystemFontOfSize(16)

    height = height + (data[:module_name].sizeWithFont(font,
      constrainedToSize: contentSize, lineBreakMode: NSLineBreakByWordWrapping).height)

    if height < MIN_HEIGHT
      height = MIN_HEIGHT # min height
    end

    height + (10 * 2)
  end

  def tapped_item(mod)
    open LessonsScreen.new(mod: mod)
  end

end