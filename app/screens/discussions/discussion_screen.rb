class DiscussionScreen < PM::Screen

  title "Discussion"
  tab_bar_item title: "Discussion", icon: "286-speechbubble"

  attr_accessor :items

  def view_did_load
    if Device.ios_version == "7.0"
      self.navigationController.navigationBar.barTintColor = '#F59800'.to_color
      self.navigationController.navigationBar.tintColor = '#99763D'.to_color
    end
  end


  def on_present
    self.navigationController.navigationBar.barTintColor = '#F59800'.to_color
    self.navigationController.navigationBar.tintColor = '#99763D'.to_color
  end

  def on_load
    @table = UITableView.alloc.initWithFrame(self.view.bounds)
    self.view.addSubview @table

    @items = []

    @table.dataSource = @table.delegate = self

    do_refresh
  end

  def will_appear
    do_refresh
  end

  def do_refresh
    UIApplication.sharedApplication.delegate.make_post("discussion_board/_ext_retrieve_discussion_threads.php",
      key: App::Persistence[SESSION_KEY],
      session_id: App::Persistence[SESSION_ID]) do |result|

      @items = result.object[:items]
      @table.reloadData

    end
  end

  def tableView(tableView, cellForRowAtIndexPath: indexPath)
    @reuseIdentifier ||= "SOPP_THREAD_CELL_IDENTIFIER"

    cell = tableView.dequeueReusableCellWithIdentifier(@reuseIdentifier) || begin
      TDBadgedCell.alloc.initWithStyle(UITableViewCellStyleSubtitle, reuseIdentifier:@reuseIdentifier)
    end

    thread = @items[indexPath.row]

    cell.badgeString = thread[:total_replies].to_s
    cell.badgeColor = '#F59800'.to_color
    cell.badge.fontSize = 14;
    cell.badge.radius = 3;

    cell.textLabel.text = thread[:discussion_topic]
    cell.detailTextLabel.text = "Last replied: #{thread[:last_replied_datetime]}"

    cell
  end

  def tableView(tableView, numberOfRowsInSection: section)
    @items.size
  end

  def tableView(tableView, didSelectRowAtIndexPath: indexPath)
    open ThreadsScreen.new(thread: @items[indexPath.row])
  end

end