class ThreadsScreen < PM::TableScreen

  refreshable

  attr_accessor :thread

  MIN_HEIGHT = 40

  def on_load
    self.title = @thread[:discussion_topic]
    do_refresh
  end

  def table_data
    @table_data ||= []
  end

  def on_present
    self.navigationController.navigationBar.barTintColor = '#F59800'.to_color
    self.navigationController.navigationBar.tintColor = '#99763D'.to_color
  end

  def on_refresh
    do_refresh
  end

  def on_appear
    add_right_bar_button
  end

  def do_refresh
    UIApplication.sharedApplication.delegate.make_post("discussion_board/_ext_read_discussion_thread.php",
      key: App::Persistence[SESSION_KEY],
      session_id: App::Persistence[SESSION_ID],
      discussion_thread_id: @thread[:discussion_thread_id]) do |result|
      @items = result.object[:items]
      @table_data = [{
        cells: @items.map do |thread|
          {
            cell_style: UITableViewCellStyleSubtitle,
            cell_identifier: "ThreadCell",
            cell_class: ThreadCell,
            height: calculate_height(thread),
            remote_image: {
              url: thread[:contact_photo],
              placeholder: 'dummy',
              size: MIN_HEIGHT,
              radius: 0
            },
            title: thread[:contact_name],
            subtitle: thread[:discussion_reply_content],
            date: thread[:discussion_reply_created_date],
            order: thread[:discussion_reply_no],
            controller: self.navigationController
          }
        end
      }]
      stop_refreshing
      update_table_data
    end
  end

  def add_right_bar_button
    self.navigationItem.rightBarButtonItem = UIBarButtonItem.titled('Reply') do
      show_reply
    end
  end

  def show_reply
    self.navigationItem.rightBarButtonItem = nil

    tempView = UIView.alloc.initWithFrame(CGRectMake(0, 0, 320, 300))
    self.view.addSubview tempView

    popupView = YIPopupTextView.alloc.initWithPlaceHolder("Your reply here",
      maxCount: 1000, buttonStyle: YIPopupTextViewButtonStyleRightCancelAndDone,
      tintsDoneButton: true)
    popupView.delegate = self
    popupView.caretShiftGestureEnabled = true
    popupView.showInView(tempView)

  end

  # YIPopupTextView Delegates
  def popupTextView(textView, willDismissWithText: text, cancelled: cancelled)
  end

  def popupTextView(textView, didDismissWithText: text, cancelled: cancelled)
    add_right_bar_button
    unless cancelled
      # Submit to the server and refresh
      UIApplication.sharedApplication.delegate.make_post("discussion_board/_ext_create_discussion_thread_reply.php",
        key: App::Persistence[SESSION_KEY],
        session_id: App::Persistence[SESSION_ID],
        discussion_thread_id: @thread[:discussion_thread_id],
        reply_content: text) do |result|
        KGStatusBar.showSuccessWithStatus(result.object[:message])
        do_refresh
      end
    end
  end


  # Helper method to calculate cell height
  def calculate_height(thread)
    height = 0

    contentWidth = 250
    contentSize = CGSizeMake(contentWidth, 1200)

    font = UIFont.boldSystemFontOfSize(16)

    height = height + (thread[:discussion_reply_content].sizeWithFont(font,
      constrainedToSize: contentSize, lineBreakMode: NSLineBreakByWordWrapping).height)

    height = height + (thread[:discussion_reply_created_date].sizeWithFont(font,
      constrainedToSize: contentSize, lineBreakMode: NSLineBreakByWordWrapping).height)

    height = height + (thread[:contact_name].sizeWithFont(font,
      constrainedToSize: contentSize, lineBreakMode: NSLineBreakByWordWrapping).height)

    if height < MIN_HEIGHT
      height = MIN_HEIGHT # min height
    end

    height + (10 * 2)
  end
end