class EditProfileScreen < PM::FormotionScreen

  attr_accessor :contact_id

  title "Edit Profile"

  def table_data
    {
    }
  end

  def on_present
    self.navigationController.navigationBar.barTintColor = '#F59800'.to_color
    self.navigationController.navigationBar.tintColor = '#99763D'.to_color
  end

  def view_did_load

    self.navigationItem.rightBarButtonItem = UIBarButtonItem.save {
      self.form.submit
    }
    UIApplication.sharedApplication.delegate.make_post("contact/_ext_read_student_profile.php",
      key: App::Persistence[SESSION_KEY],
      session_id: App::Persistence[SESSION_ID]) do |result|
      @item = result.object[:items][0]

      # Download contact photo
      manager = SDWebImageManager.sharedManager
      manager.downloadWithURL(@item[:contact_photo],
        options: 0,
        progress: lambda { |receivedSize, expectedSize|
        },
        completed: lambda { |image, error, cacheType, data|
          @contact_photo_image = image
          form = Formotion::Form.new
          form.title = "Edit Profile"

          form.build_section do |section|
            section.title = "Personal Particulars"
            section.build_row do |row|
              row.title = "Profile Photo"
              row.key = :contact_photo
              row.type = :image
              row.value = @contact_photo_image
              row.return_key = :done
              row.editable = true
            end
            section.build_row do |row|
              row.title = "Student No"
              row.value = @item[:student_no]
              row.key = :student_no
              row.type = :number
              row.return_key = :done
              row.editable = false
            end
            section.build_row do |row|
              row.title = "First Name"
              row.value = @item[:contact_first_name]
              row.key = :contact_first_name
              row.type = :string
              row.return_key = :done
            end
            section.build_row do |row|
              row.title = "Last Name"
              row.value = @item[:contact_last_name]
              row.key = :contact_last_name
              row.type = :string
              row.return_key = :done
            end
            section.build_row do |row|
              row.title = "Salutation"
              row.value = @item[:contact_salutation]
              row.key = :contact_salutation
              row.type = :picker_with_done
              row.items = ["Dr", "Mr", "Mdm", "Ms"]
            end

            section.build_row do |row|
              row.title = "Date of Birth"
              row.value = @item[:contact_dob]
              row.key = :contact_dob
              row.type = :date_with_done
              row.format = :short
              row.return_key = :done
              @date_row = row
            end

            section.build_row do |row|
              row.title = "NRIC"
              row.value = @item[:contact_nric]
              row.key = :contact_nric
              row.type = :string
              row.return_key = :done
            end
            section.build_row do |row|
              row.title = "Nationality"
              row.value = @item[:contact_nationality]
              row.key = :contact_nationality
              row.type = :picker_with_done
              row.items = ["Singapore Citizen", "Singapore PR", "Others"]
            end
            section.build_row do |row|
              row.title = "Nationality (Others)"
              row.value = @item[:contact_nationality_others]
              row.key = :contact_nationality_others
              row.type = :string
              row.return_key = :done
            end
          end

          form.build_section do |section|
            section.title = "Education Details"
            section.build_row do |row|
              row.title = "Qualification"
              row.value = @item[:contact_qualification]
              row.key = :contact_qualification
              row.type = :string
              row.type = :picker_with_done
              row.items = ["Secondary", "Tertiary ", "Graduate", "Post-graduate", "Professional", "Others"]
            end

            section.build_row do |row|
              row.title = "Course Discipline"
              row.value = @item[:contact_course_discipline]
              row.key = :contact_course_discipline
              row.type = :picker_with_done
              row.items = ["Accountancy", "Business ", "Counselling", "Education", "Law", "Marketing", "Mass Communications", "Nursing", "Political Science", "Public Relations", "Social work"]
            end
            section.build_row do |row|
              row.title = "Others"
              row.value = @item[:contact_course_discipline_others]
              row.key = :contact_course_discipline_others
              row.type = :string
              row.return_key = :done
            end
          end
          form.build_section do |section|
            section.title = "Contact Details"
            section.build_row do |row|
              row.title = "Address"
              row.value = @item[:contact_address]
              row.key = :contact_address
              row.type = :string
              row.return_key = :done
            end
            section.build_row do |row|
              row.title = "Home"
              row.value = @item[:contact_home_no]
              row.key = :contact_home_no
              row.type = :number
              row.return_key = :done
            end
            section.build_row do |row|
              row.title = "Work"
              row.value = @item[:contact_work_no]
              row.key = :contact_work_no
              row.type = :number
              row.return_key = :done
            end
            section.build_row do |row|
              row.title = "Mobile"
              row.value = @item[:contact_mobile_no]
              row.key = :contact_mobile_no
              row.type = :number
              row.return_key = :done
            end
            section.build_row do |row|
              row.title = "Email"
              row.value = @item[:contact_email]
              row.key = :contact_email
              row.type = :email
              row.return_key = :done
              row.editable = false
            end
            section.build_row do |row|
              row.title = "Secondary"
              row.value = @item[:contact_sec_email]
              row.key = :contact_sec_email
              row.type = :email
              row.return_key = :done
            end
          end

          form.controller = self

          self.form = form
          self.form.reload_data

          self.form.on_submit do
            values = self.form.render
            submit_form(values)
          end
        }
      )
    end
  end

  def submit_form(values)
    values[:key] = App::Persistence[SESSION_KEY]
    values[:session_id] = App::Persistence[SESSION_ID]
    date_formatter = NSDateFormatter.alloc.init

    unless @item[:contact_dob] == @date_row.text_field.text
      date_formatter.dateFormat = "d/m/y"
      date = date_formatter.dateFromString @date_row.text_field.text
      date_formatter.dateFormat = "m/d/yy"
      values[:contact_dob] = date_formatter.stringFromDate date
    end

    unless values[:contact_photo].nil?
      values[:contact_photo] = UIImageJPEGRepresentation(values[:contact_photo], 0.8).base64Encoding
      UIApplication.sharedApplication.delegate.make_post("contact/_ext_update_contact_photo.php",
        values) do |result|
        p result
      end
    end

    values[:contact_photo] = nil
    values[:contact_id] = @contact_id
    UIApplication.sharedApplication.delegate.make_post("contact/_ext_update_student_profile.php",
      values) do |result|
      close
    end

  end

end