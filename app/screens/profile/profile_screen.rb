class ProfileScreen < PM::FormotionScreen

  title "Profile"
  tab_bar_item title: "Profile", icon: "111-user"

  item = nil
  contact_photo_image = nil

  def table_data
    {
    }
  end

  def on_present
    self.navigationController.navigationBar.barTintColor = '#F59800'.to_color
    self.navigationController.navigationBar.tintColor = '#99763D'.to_color
  end

  def view_did_load
    if Device.ios_version == "7.0"
      self.navigationController.navigationBar.barTintColor = '#F59800'.to_color
      self.navigationController.navigationBar.tintColor = '#99763D'.to_color
    end
  end

  def will_appear

    self.navigationItem.rightBarButtonItem = UIBarButtonItem.edit {
      open_edit_screen
    }

    UIApplication.sharedApplication.delegate.make_post("contact/_ext_read_student_profile.php",
      key: App::Persistence[SESSION_KEY],
      session_id: App::Persistence[SESSION_ID]) do |result|
      @item = result.object[:items][0]

      @contact_id = @item[:contact_id]

      # Download contact photo
      manager = SDWebImageManager.sharedManager
      manager.downloadWithURL(@item[:contact_photo],
        options: 0,
        progress: lambda { |receivedSize, expectedSize|
        },
        completed: lambda { |image, error, cacheType, data|
          @contact_photo_image = image
          # Build a form
          form = Formotion::Form.new
          form.title = "Profile"

          form.build_section do |section|
            section.title = "Personal Particulars"
            section.build_row do |row|
              row.title = "Profile Photo"
              row.key = :contact_photo
              row.type = :image
              row.value = @contact_photo_image
              row.return_key = :done
              row.editable = false
            end
            section.build_row do |row|
              row.title = "Student No"
              row.value = @item[:student_no]
              row.key = :student_no
              row.type = :number
              row.return_key = :done
              row.editable = false
            end
            section.build_row do |row|
              row.title = "First Name"
              row.value = @item[:contact_first_name]
              row.key = :contact_first_name
              row.type = :string
              row.return_key = :done
              row.editable = false
            end
            section.build_row do |row|
              row.title = "Last Name"
              row.value = @item[:contact_last_name]
              row.key = :contact_last_name
              row.type = :string
              row.return_key = :done
              row.editable = false
            end
            section.build_row do |row|
              row.title = "Salutation"
              row.value = @item[:contact_salutation]
              row.key = :contact_salutation
              row.type = :picker_with_done
              row.items = ["Dr", "Mr", "Mdm", "Ms"]
              row.editable = false
            end

            ## Convert the date format to m/d/y

            section.build_row do |row|
              row.title = "Date of Birth"
              row.value = @item[:contact_dob]
              row.key = :contact_dob
              row.type = :string
              row.return_key = :done
              row.editable = false
            end

            section.build_row do |row|
              row.title = "NRIC"
              row.value = @item[:contact_nric]
              row.key = :contact_nric
              row.type = :string
              row.return_key = :done
              row.editable = false
            end
            section.build_row do |row|
              row.title = "Nationality"
              row.value = @item[:contact_nationality]
              row.key = :contact_nationality
              row.type = :string
              row.return_key = :done
              row.editable = false
            end
            section.build_row do |row|
              row.title = "Nationality (Others)"
              row.value = @item[:contact_nationality_others]
              row.key = :contact_nationality_others
              row.type = :string
              row.return_key = :done
              row.editable = false
            end
          end
          form.build_section do |section|
            section.title = "Education Details"
            section.build_row do |row|
              row.title = "Qualification"
              row.value = @item[:contact_qualification]
              row.key = :contact_qualification
              row.type = :string
              row.return_key = :done
              row.editable = false
            end

            section.build_row do |row|
              row.title = "Course Discipline"
              row.value = @item[:contact_course_discipline]
              row.key = :contact_course_discipline
              row.type = :string
              row.return_key = :done
              row.editable = false
            end
            section.build_row do |row|
              row.title = "Others"
              row.value = @item[:contact_course_discipline_others]
              row.key = :contact_course_discipline_others
              row.type = :string
              row.return_key = :done
              row.editable = false
            end
          end
          form.build_section do |section|
            section.title = "Contact Details"
            section.build_row do |row|
              row.title = "Address"
              row.value = @item[:contact_address]
              row.key = :contact_address
              row.type = :string
              row.return_key = :done
              row.editable = false
            end
            section.build_row do |row|
              row.title = "Home"
              row.value = @item[:contact_home_no]
              row.key = :contact_home_no
              row.type = :number
              row.return_key = :done
              row.editable = false
            end
            section.build_row do |row|
              row.title = "Work"
              row.value = @item[:contact_work_no]
              row.key = :contact_work_no
              row.type = :number
              row.return_key = :done
              row.editable = false
            end
            section.build_row do |row|
              row.title = "Mobile"
              row.value = @item[:contact_mobile_no]
              row.key = :contact_mobile_no
              row.type = :number
              row.return_key = :done
              row.editable = false
            end
            section.build_row do |row|
              row.title = "Email"
              row.value = @item[:contact_email]
              row.key = :contact_email
              row.type = :email
              row.return_key = :done
              row.editable = false
            end
            section.build_row do |row|
              row.title = "Secondary"
              row.value = @item[:contact_sec_email]
              row.key = :contact_sec_email
              row.type = :email
              row.return_key = :done
              row.editable = false
            end
          end

          form.controller = self

          self.form = form
          self.form.reload_data

        }
      )
    end
  end

  def open_edit_screen
    open EditProfileScreen.new contact_id: @contact_id
  end

end