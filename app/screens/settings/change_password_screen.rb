class ChangePasswordScreen < PM::FormotionScreen
  title "Change Password"
  def table_data
    {
      sections: [{
        rows: [{
          title: "Old Password",
          type: :string,
          secure: true,
          key: :old
        }, {
          title: "New Password",
          type: :string,
          secure: true,
          key: :new
        }]
      }, {
        rows: [{
          title: "Save",
          type: :submit
        }]
      }]
    }
  end

  def on_present
    self.navigationController.navigationBar.barTintColor = '#F59800'.to_color
    self.navigationController.navigationBar.tintColor = '#99763D'.to_color
  end

  def on_load
    self.form.on_submit do
      values = self.form.render
      UIApplication.sharedApplication.delegate.make_post("client/_ext_change_password.php",
        key: App::Persistence[SESSION_KEY],
        session_id: App::Persistence[SESSION_ID],
        prev_password: values[:old],
        new_password: values[:new]) do |result|
        close
      end
    end
  end
end