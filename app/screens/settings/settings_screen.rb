class SettingsScreen < PM::FormotionScreen

  title "Settings"
  tab_bar_item title: "Settings", icon: "19-gear"


  def on_present
    self.navigationController.navigationBar.barTintColor = '#F59800'.to_color
    self.navigationController.navigationBar.tintColor = '#99763D'.to_color
  end

  def table_data
    form = Formotion::Form.new
    form.build_section do |section|
      section.build_row do |row|
        row.title = "Change Password"
        row.type = "submit"
        row.on_tap do
          open ChangePasswordScreen.new
        end
      end

      section.build_row do |row|
        row.title = "Logout"
        row.type = :submit
        row.on_tap do
          App::Persistence[SESSION_ID] = nil
          open_root_screen LoginScreen.new(nav_bar: true)
        end
      end
    end
    form
  end
end