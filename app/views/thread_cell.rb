class ThreadCell < PM::TableViewCell

  include ProMotion::TableViewCellModule
  attr_accessor :dateLabel, :orderLabel, :contentLabel, :nav_controller

  def initWithStyle(style, reuseIdentifier:cell_identifier)
    super

      @dateLabel = UILabel.alloc.init
      @dateLabel.font = UIFont.systemFontOfSize 16
      @dateLabel.textColor = UIColor.blackColor

      @orderLabel = UILabel.alloc.init
      @orderLabel.font = UIFont.systemFontOfSize 12
      @orderLabel.textColor = UIColor.grayColor

      self.textLabel.font = UIFont.boldSystemFontOfSize 14
      self.textLabel.textColor = UIColor.grayColor

      @contentLabel = RTLabel.alloc.init
      @contentLabel.textColor = UIColor.blackColor
      @contentLabel.font = UIFont.systemFontOfSize 14

      self.detailTextLabel.hidden = true

      self.contentView.addSubview(@dateLabel)
      self.contentView.addSubview(@orderLabel)
      self.contentView.addSubview(@contentLabel)
      @contentLabel.delegate = self
    # self.userInteractionEnabled = false
    self
  end

  def setup(data_cell, screen)
    super
    set_date
    set_order
    @contentLabel.text = data_cell[:subtitle]
    @nav_controller = data_cell[:controller]
  end

  def layoutSubviews
    super

    self.detailTextLabel.numberOfLines = 0
    self.detailTextLabel.lineBreakMode = NSLineBreakByWordWrapping

    maxSize = CGSizeMake 200, 9999

    self.orderLabel.frame = CGRectMake 5, 10, 20, 10
    self.imageView.frame = CGRectMake 40, 0, 60, 60
    self.textLabel.frame = CGRectMake 110, 5, 200, 30
    self.dateLabel.frame = CGRectMake 110, 25, 200, 30

    expectedLabelSize = @data_cell[:subtitle].sizeWithFont(
      @contentLabel.font, constrainedToSize: maxSize,
      lineBreakMode: @contentLabel.lineBreakMode)

    @contentLabel.frame = CGRectMake 110, 50, expectedLabelSize.width, expectedLabelSize.height
  end

  def rtLabel(rtLabel, didSelectLinkWithURL: url)
    p 'Clicking'
    webViewController = SVWebViewController.alloc.initWithAddress url.to_s
    @nav_controller.pushViewController webViewController, animated: true
  end


  def set_date
    if data_cell[:date] && self.dateLabel
      if data_cell[:date].is_a? NSAttributedString
        self.dateLabel.attributedText = data_cell[:date]
      else
        self.dateLabel.text = data_cell[:date]
      end
      self.dateLabel.backgroundColor = UIColor.clearColor
      self.dateLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth
    end
    self
  end

  def set_order
    if data_cell[:order] && self.dateLabel
      if data_cell[:order].is_a? NSAttributedString
        self.orderLabel.attributedText = data_cell[:order]
      else
        self.orderLabel.text = data_cell[:order]
      end
      self.orderLabel.backgroundColor = UIColor.clearColor
      self.orderLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth
    end
    self
  end

end